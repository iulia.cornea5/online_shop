package org.example.shop;

import org.example.shop.entity.Product;
import org.example.shop.entity.ProductCategory;
import org.example.shop.entity.Review;
import org.example.shop.entity.Tag;
import org.example.shop.repository.ProductCategoryRepository;
import org.example.shop.repository.ProductRepository;
import org.example.shop.repository.ReviewRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        System.out.println("Creating session factory");
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Product.class)
                .addAnnotatedClass(ProductCategory.class)
                .addAnnotatedClass(Review.class)
                .addAnnotatedClass(Tag.class)
                .buildSessionFactory();
        System.out.println("Session factory created");

        try {

            ProductCategoryRepository categoryRepository =
                    new ProductCategoryRepository(sessionFactory);
            ProductCategory alimente =
                    new ProductCategory(null, "Produse alimentare");
            System.out.println(categoryRepository.create(alimente));

            Product orez = new Product(null, "Orez", 12d, 100, alimente);
            Product faina = new Product(null, "Faina", 5.5, 100, alimente);
            ProductRepository productRepository = new ProductRepository(sessionFactory);
            productRepository.create(orez);
            productRepository.create(faina);
            System.out.println(productRepository.findAll());

            System.out.println(categoryRepository.findById(faina.getProductCategory().getId()));

            ProductCategory electronice = new ProductCategory(null, "electronice");
            System.out.println(categoryRepository.create(electronice));

            Product p1 = new Product(null, "Uscator de par", 150d, 12, electronice);
            Product p2 = new Product(null, "Aspirator", 300d, 5, electronice);
            electronice.setProducts(List.of(p1,p2));

            productRepository.create(p1);
            productRepository.create(p2);
            System.out.println(productRepository.findAll());


            Review r1 = new Review(null, 9, "Good product!", "iulia@gmail.com", p1);
            Review r11 = new Review(null, 8, "Good enough product!", "george@gmail.com", p1);
            Review r2 = new Review(null, 3, "Really bad product!", "geroge@gmail.com", p2);

            Tag goodPrice = new Tag("good price", true);
            Tag qualityMaterials = new Tag("quality materials", true);
            Tag fastShipping = new Tag ("fast shipping", true);
            Tag lateShipping = new Tag ("late shipping", false);
            Tag poorQuality = new Tag ("poor quality", false);

            ReviewRepository reviewRepository = new ReviewRepository(sessionFactory);
            r1.addTag(goodPrice);
            r1.addTag(qualityMaterials);
            reviewRepository.add(r1);
            r11.addTag(qualityMaterials);
            r11.addTag(fastShipping);
            reviewRepository.add(r11);
            r2.addTag(lateShipping);
            r2.addTag(poorQuality);
            reviewRepository.add(r2);

//            Session s = sessionFactory.openSession();
//            Transaction t = s.beginTransaction();
//            s.persist(r1);
//            s.persist(r11);
//            s.persist(r2);
//            t.commit();
//            s.close();

            System.out.println(reviewRepository.findAll());


        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            sessionFactory.close();
        }


    }


}