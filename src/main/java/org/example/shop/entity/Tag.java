package org.example.shop.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Tag {

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;


    private String name;

    // true - positive
    // false - negative
    // null - neutral
    private Boolean state;

    // the name of the field which represents this entity in "Review"
    @ManyToMany(mappedBy = "tags", targetEntity = Review.class, fetch = FetchType.EAGER)
    private Set<Review> reviews = new HashSet<>();

    public Tag() {
    }

    public Tag(String name, Boolean state) {
        this.name = name;
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        if (id == null && tag.id == null) return Objects.equals(tag.getName(), name);
        return Objects.equals(id, tag.id);
    }

    @Override
    public int hashCode() {
        if (id == null) return Objects.hash(name);
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Tag{" +
                "name='" + name + '\'' +
                ", state=" + state +
                ", reviews=" + reviews.stream().map(r -> r.getId()).collect(Collectors.toList()) +
                '}';
    }
}
