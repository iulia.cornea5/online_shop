package org.example.shop.entity;

import javax.persistence.*;
import java.util.*;

@Entity
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Integer rate;

    private String comment;

    private String email;

    @JoinColumn(name = "product_id", updatable = true, insertable = true)
    @ManyToOne(targetEntity = Product.class)
    private Product product;

    public Set<Tag> getTags() {
        return tags;
    }

    // to persist tags inside a review
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "review_tag",
                joinColumns = @JoinColumn(name = "review_id"), // column that references id of current entity
                inverseJoinColumns = @JoinColumn(name = "tag_name", updatable = true)) // column that references id of the other entity in the manyToMany relationship
    private Set<Tag> tags = new HashSet<>();

    public Review() {
    }

    public Review(Integer id, Integer rate, String comment, String email, Product product) {
        this.id = id;
        this.rate = rate;
        this.comment = comment;
        this.email = email;
        this.product = product;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void addTag(Tag tag) {
        tags.add(tag); // review are un tag nou
        tag.getReviews().add(this); // tag-ul apare în acest review
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review = (Review) o;
        return Objects.equals(id, review.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", rate=" + rate +
                ", comment='" + comment + '\'' +
                ", email='" + email + '\'' +
                ", product=" + product +
                ", tags=" + tags +
                '}';
    }
}

