package org.example.shop.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.example.shop.entity.Product;

import java.util.List;

public class ProductRepository {

    private SessionFactory sessionFactory;


    public ProductRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Product create(Product p) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(p);
        transaction.commit();
        session.close();
        return p;
    }

    public void delete(Product p) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(p);
        transaction.commit();
        session.close();
    }

    public List<Product> findAll() {
        Session session = sessionFactory.openSession();
        List<Product> products = session.createQuery("from Product", Product.class).getResultList();
        session.close();
        return products;
    }
}
