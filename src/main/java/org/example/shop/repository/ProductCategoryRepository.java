package org.example.shop.repository;

import org.example.shop.entity.ProductCategory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

// CRUD - Create - Read - Update - Delete

public class ProductCategoryRepository {

    private SessionFactory sessionFactory;

    public ProductCategoryRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public ProductCategory create(ProductCategory pc) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(pc);
        transaction.commit();
        session.close();
        return pc; // îl va returna cu id-ul asignat de hibernate
    }

    public List<ProductCategory> findAll() {
        Session session = sessionFactory.openSession();
        List<ProductCategory> categories = session.createQuery("from ProductCategory", ProductCategory.class).getResultList();
        session.close();
        return categories;
    }

    public ProductCategory findById(Integer id) {
        Session session = sessionFactory.openSession();
        ProductCategory category = session
                .createQuery("select pc from ProductCategory pc where pc.id = :id", ProductCategory.class)
                .setParameter("id", id)
                .uniqueResult();
        session.close();
        return category;
    }

}
