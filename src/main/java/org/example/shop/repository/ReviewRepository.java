package org.example.shop.repository;

import org.example.shop.entity.Product;
import org.example.shop.entity.Review;
import org.example.shop.entity.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class ReviewRepository {


    private SessionFactory sessionFactory;


    public ReviewRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Review add(Review r) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.merge(r);
        transaction.commit();
        session.close();
        return r;
    }

    public List<Review> findAll() {
        Session session = sessionFactory.openSession();
        List<Review> reviews = session.createQuery("from Review", Review.class).getResultList();
        session.close();
        return reviews;
    }
}
